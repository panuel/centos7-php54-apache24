FROM centos:7

RUN yum -y update --disableplugin=fastestmirror && \
    yum -y --disableplugin=fastestmirror --setopt=tsflags=nodocs install \
      httpd \
      mod_ssl \
      php \
      php-xml \
      php-mbstring \
      php-pdo \
      php-gd \
      php-mysqlnd \
      sudo \
      openssl \
    yum clean all && \
    rm -rf /var/cache/yum 
 
RUN echo date.timezone = Europe/Berlin >> /etc/php.ini

RUN ln -sf /dev/stdout /var/log/httpd/access_log && \
    ln -sf /dev/stderr /var/log/httpd/error_log

ADD run-httpd.sh /run-httpd.sh

EXPOSE 80 443

CMD ["/run-httpd.sh"]
